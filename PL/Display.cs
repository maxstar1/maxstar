﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;

namespace PL
{
    public abstract class Display// a interface that display a menu on the screen
    {
        public PrintList printList = new PrintList();

        abstract public void displayMenu();
        abstract public void ParseCmd(String cmd);

        public String ReceiveCmd()
        {
            return Console.ReadLine();
        }
    }
}
