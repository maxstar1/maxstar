﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;

namespace PL
{
    public class PL_CLI : IPL
    {
        public MainMenu mainMenu;
        public Stack<Display> undo;
        public IBL aBL;

        public PL_CLI(IBL aBL)
        {
            this.aBL = aBL;
            undo = new Stack<Display>();
            mainMenu = new MainMenu(aBL);
        }

        public void run()
        {
            mainMenu.displayMenu();
            mainMenu.ParseCmd(ReceiveCmd());
        }

        public String ReceiveCmd()
        {
            return Console.ReadLine();
        }
    }
}
