﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;
using BL;

namespace PL
{
    public class Edit : Display
    {
        private String name;
        //public Display display;//@ could be DoctorMenu or PatientMenu
        public MainMenu mainMenu;
        public IBL aBL;

        public Edit(String name, MainMenu mainMenu)
        {
            this.name = name;
            this.mainMenu = mainMenu;
            aBL = mainMenu.aBL;
        }

        override public void displayMenu()
        {
            Console.WriteLine("Edit "+name+" Menu");
            Console.WriteLine("------------------");
            Console.WriteLine("1) Add "+name);
            Console.WriteLine("2) Remove "+name);
            Console.WriteLine("3) Back to "+name+" Menu");
            Console.WriteLine("4) Back to Main Menu");
        }

        override public void ParseCmd(String cmd)
        {
            switch (cmd)
            {
                case "1": //@Add 
                    mainMenu.back.Push(this);
                    Console.Clear();
                    aBL.add(name);
                    Console.WriteLine("Press any key to continue");
                    Console.ReadLine();
                    Console.Clear();
                    mainMenu.goBack();
                    break;

                case "2": //@Remove
                    mainMenu.back.Push(this);
                    Console.Clear();
                   // bool ans = aBL.removeById(name);
                    if (aBL.removeById(name) == true)
                    {
                        Console.WriteLine("The " + name + " removed successfully");
                        Console.WriteLine("Press any key to continue");
                        Console.ReadLine();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        Console.WriteLine(name + " not found");
                        Console.WriteLine("Press any key to continue");
                        Console.ReadLine();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    break;

                case "3":
                    Console.Clear();
                    mainMenu.goBack();
                    break;
                case "4":
                    Console.Clear();
                    mainMenu.returnToMainMenu();
                    break;

                default:
                    Console.WriteLine("Invalid selection. Please select 1, 2, 3 or 4.");
                    this.ParseCmd(ReceiveCmd());
                    break;
            }
        }

        public void displayEditMenu()
        {
            displayMenu();
            ParseCmd(ReceiveCmd());
        }
    }
}
