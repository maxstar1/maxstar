﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;
using BL_BackHand;

namespace PL
{
    public class VisitsMenu : Display
    {
        public MainMenu mainMenu;
        public IBL aBL;

        public VisitsMenu(MainMenu mainMenu)
        {
            this.mainMenu = mainMenu;
            aBL = mainMenu.aBL;
        } 

        override public void displayMenu()
        {
            Console.WriteLine("Visits Menu");
            Console.WriteLine("-----------");
            Console.WriteLine("1) List of all visits");
            Console.WriteLine("2) Find specific visit");
            Console.WriteLine("3) Back to Main Menu");
        }

        override public void ParseCmd(String cmd)
        {
            switch (cmd)
            {
                case "1": //@ List of all visits
                    mainMenu.back.Push(this);
                    Console.Clear();
                    if (aBL.visitsByDateV() == null)
                    {
                        Console.WriteLine("List is empty");
                        Console.WriteLine("Press any key to continue");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.printVT(aBL.visitsByDateV());
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    break;

                case "2": // @Find specific visit
                    mainMenu.back.Push(this);
                    Console.Clear();
                    Visit v = aBL.getSpecificByIdV();
                    if (v == null)
                    {
                        Console.Clear();
                        Console.WriteLine("visit not found");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.specificPrint(v);
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    break;

                case "3":
                    Console.Clear();
                    mainMenu.goBack();
                    break;

                default:
                    Console.WriteLine("Invalid selection. Please select 1, 2 or 3.");
                    this.ParseCmd(ReceiveCmd());
                    break;
            }
        }
    }
}
