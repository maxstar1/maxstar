﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;
using BL;

namespace PL
{
    public class DoctorMenu : Display
    {
        public MainMenu mainMenu;
        public Edit edit;
        public IBL aBL;

        public DoctorMenu(MainMenu mainMenu)
        {
            this.mainMenu = mainMenu;
            aBL = mainMenu.aBL;
        } 

        override public void displayMenu()
        {
            Console.WriteLine("Doctor Menu");
            Console.WriteLine("-----------");
            Console.WriteLine("1) List of doctors by firstName");
            Console.WriteLine("2) List of doctors by LastName");
            Console.WriteLine("3) List of doctors by Salary");
            Console.WriteLine("4) Show Doctor profile");
            Console.WriteLine("5) Edit");
            Console.WriteLine("6) Back to Main Menu");
        }

        override public void ParseCmd(String cmd)
        {
            switch (cmd)
            {
                case "1":
                    mainMenu.back.Push(this);
                    Console.Clear();
                    if (aBL.getByName("Doctor").Count==0) 
                    {
                        Console.WriteLine("List is empty");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.print(aBL.getByName("Doctor"));
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    break;
                    
                case "2":
                    mainMenu.back.Push(this);
                    Console.Clear();
                    if (aBL.getByLname("Doctor").Count==0)
                    {
                        Console.WriteLine("List is empty");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.print(aBL.getByLname("Doctor"));
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    break;

                case "3":
                    mainMenu.back.Push(this);
                    Console.Clear();
                    if (aBL.getBySalary().Count==0)
                    {
                        Console.WriteLine("List is empty");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.print(aBL.getBySalary().ConvertAll(i => i as Person));
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    break;

                case "4":
                    mainMenu.back.Push(this);
                    Console.Clear();
                    Doctor doc = aBL.FindById();
                    if (doc==null)
                    {
                        Console.Clear();
                        Console.WriteLine("Doctor not found");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        displayMenu();
                        ParseCmd(ReceiveCmd());
                    }
                    else
                    {
                        DoctorProfileMenu profile = new DoctorProfileMenu(mainMenu, doc);
                        profile.displayMenu();
                        profile.ParseCmd(ReceiveCmd());
                    }
                    break;

                case "5":
                    edit = new Edit("Doctor", mainMenu);
                    mainMenu.back.Push(this);
                    Console.Clear();
                    edit.displayEditMenu();
                    break;

                case "6":
                    Console.Clear();
                    mainMenu.returnToMainMenu();
                    break;

                default:
                    Console.WriteLine("Invalid selection. Please select 1, 2, 3, 4, 5 or 6.");
                    this.ParseCmd(ReceiveCmd());
                    break;
            }
        }
    }
}
