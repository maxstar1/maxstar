﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;
using BL;

namespace PL
{
    public class TreatmentMenu : Display
    {
        public MainMenu mainMenu;
        public IBL aBL;

        public TreatmentMenu(MainMenu mainMenu)
        {
            aBL = mainMenu.aBL;
            this.mainMenu = mainMenu;
        } 

        override public void displayMenu()
        {
            Console.WriteLine("Treatment Menu");
            Console.WriteLine("--------------");
            Console.WriteLine("1) List of all treatments");
            Console.WriteLine("2) Find specific treatmrnt");
            Console.WriteLine("3) Back to Main Menu");
        }

        override public void ParseCmd(String cmd)
        {
            switch (cmd)
            {
                case "1": //@List of all treatments
                    mainMenu.back.Push(this);
                    Console.Clear();
                    if (aBL.treatmentByDateT().Count == 0)
                    {
                        Console.WriteLine("List is empty");
                        Console.WriteLine("Press any key to continue");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.print(aBL.treatmentByDateT());

                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    break;

                case "2": //@Find specific treatmrnt
                    mainMenu.back.Push(this);
                    Console.Clear();
                    if (aBL.getSpecificByIdT()== null)
                    {
                        Console.Clear();
                        Console.WriteLine("Treatmrnt not found");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                        
                    }
                    else
                    {
                        printList.specificPrint(aBL.getSpecificByIdT());
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    break;

                case "3":
                    Console.Clear();
                    mainMenu.goBack();
                    break;

                default:
                    Console.WriteLine("Invalid selection. Please select 1, 2 or 3.");
                    this.ParseCmd(ReceiveCmd());
                    break;
            }
        }
    }
}
