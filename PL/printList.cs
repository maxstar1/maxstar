﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;

namespace PL
{
    public class PrintList
    {
        public void print(List<Person> list)
            //@ pairnts list of patiens
        {
            
            if (list.First() is Patient)
            {
                List<Patient> listP = list.ConvertAll(i => i as Patient);
                Console.WriteLine("List of all Patients:");
                foreach (Patient p in listP)
                {
                    
                    Console.WriteLine(p.firstName+" "+p.lastName+"  "+p.id+"  "+p.age+"  "+p.gender+"  "+"Dr."+p.mainDoctor.firstName+" "+p.mainDoctor.lastName);
                }
            }
            if (list.First() is Doctor)
            //@ pairnts list of doctors
            {
                List<Doctor> listP = list.ConvertAll(i => i as Doctor);
                Console.WriteLine("List of all Doctors:");
                foreach (Doctor d in listP)
                {
                    Console.WriteLine(d.firstName + " " + d.lastName +"  "+d.id+"  " + d.age + "  " + d.gender + "  " + d.salary);
                }
            }
        }
        public void printVT(List<Visit> list)
        { //@ pairnts list of doctors
            {
                List<Visit> listP = list.ConvertAll(i => i as Visit);
                Console.WriteLine("List of all Visits:");
                foreach (Visit v in listP)
                {
                    Console.WriteLine(v.visitID+"  "+v.PatientID+"  "+v.dateOfVisit+"  "+"Dr."+v.AssignedDoctor.firstName+" "+v.AssignedDoctor.lastName);
                }
            }
        }
        public void print(List<Treatment> list)
        { //@ pairnts list of doctors
            {
                List<Treatment> listP = list.ConvertAll(i => i as Treatment);
                Console.WriteLine("List of all Treatments:");
                foreach (Treatment t in listP)
                {
                    Console.WriteLine(t.DateOfStart+"  "+t.DateOfFinish+"  "+"Dr."+t.createdByDoctor.firstName+" "+t.createdByDoctor.lastName);
                }
            }
        }
            
        public void specificPrint(Object obj)
        {
            if (obj is Visit)
            {
                Visit v = (Visit)obj;
                Console.WriteLine("Patient ID: "+v.PatientID + "  Date:" + v.dateOfVisit);
                Console.WriteLine("Assigned Doctor: " + v.AssignedDoctor.firstName + " " + v.AssignedDoctor.lastName);
                Console.WriteLine("Doctor notes: " + v.DoctorNotes);
            }

            if (obj is Treatment)
            {
                Treatment t = (Treatment)obj;
                Console.WriteLine("Date of start: " + t.DateOfStart + "  Date of finish:" + t.DateOfFinish);
                Console.WriteLine("Assigned Doctor: " + t.createdByDoctor.firstName + " " + t.createdByDoctor.lastName);
                Console.WriteLine("Doctor Prognosis: " + t.Prognosis);
                Console.WriteLine("Prescriptions: " + t.Prescriptions);
            }
        }
            
    }
}

