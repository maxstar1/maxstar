﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;
using BL;

namespace PL
{
    public class PatientProfileMenu : Display
    // Display Patient profile
    {
        public MainMenu mainMenu;
        private Patient patient;
        public IBL aBL;

        public PatientProfileMenu( MainMenu mainMenu, Patient patient)
        {
            aBL = mainMenu.aBL;
            this.mainMenu = mainMenu;
            this.patient = patient;
        }

        override public void displayMenu()
        {
            Console.WriteLine("Patient Profile");
            Console.WriteLine("========================");

                if (patient.gender.Equals("male"))
                {
                    Console.WriteLine("Name: Mrs. "+patient.firstName+" "+patient.lastName);
                }
                else
                {
                    Console.WriteLine("Name: Ms."+patient.firstName+" "+patient.lastName);
                }
            Console.WriteLine("Gender: "+patient.gender);
            Console.WriteLine("Age: "+patient.age);
            Console.WriteLine("Doctor: " + patient.mainDoctor.firstName +" "+patient.mainDoctor.lastName);
            Console.WriteLine("------------------------");
            Console.WriteLine("Patient Details");
            Console.WriteLine();
            Console.WriteLine("1) History of visits");
            Console.WriteLine("2) History of treatments");
            Console.WriteLine("3) Edit main doctor");
            Console.WriteLine("4) Add visit");
            Console.WriteLine("5) Add treatment");
            Console.WriteLine("6) Back to Doctor Menu");
            Console.WriteLine("7) Back to Main Menu");
        }
        

        override public void ParseCmd(String cmd)
        {
            switch (cmd)
            {
                case "1": //@print History of visits
                    mainMenu.back.Push(this);
                    Console.Clear();
                    if (aBL.getVisitsP(patient).Count == 0)
                    {
                        Console.WriteLine("List is empty");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.printVT(aBL.getVisitsP(patient));
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        mainMenu.goBack();
                    }
                    break;

                case "2": //@print History of treatments
                    mainMenu.back.Push(this);
                    Console.Clear();
                    if (aBL.getTreatmentP(patient).Count == 0)//@checks if list is empty
                    {
                        Console.WriteLine("List is empty");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.print(aBL.getTreatmentP(patient));
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        mainMenu.goBack();
                    }
                    break;

                case "3": //@Edit main doctor
                    mainMenu.back.Push(this);
                    Console.Clear();
                    aBL.changeDetails("Patient", patient.id);
                    Console.WriteLine("Main doctor changed successfully press any key to refresh");
                    Console.Clear();
                    this.displayMenu();
                    this.ParseCmd(ReceiveCmd());
                    break;

                case "4": //@Add visit
                    mainMenu.back.Push(this);
                    Console.Clear();
                    aBL.add("Visit");
                    Console.WriteLine("Press any key to continue");
                    ReceiveCmd();
                    Console.Clear();
                    mainMenu.goBack();
                    break;

                case "5": //@Add treatment
                    mainMenu.back.Push(this);
                    Console.Clear();
                    aBL.add("Treatment");
                    Console.WriteLine("you successfuly add a new Treatment");
                    Console.WriteLine("Press any key to continue");
                    ReceiveCmd();
                    Console.Clear();
                    mainMenu.goBack();
                    break;

                case "6":
                    Console.Clear();
                    mainMenu.goBack();
                    break;

                case "7":
                    Console.Clear();
                    mainMenu.returnToMainMenu();
                    break;

                default:
                    Console.WriteLine("Invalid selection. Please select 1, 2, 3, 4, 5, 6 or 7.");
                    this.ParseCmd(ReceiveCmd());
                    break;
            }
        }
    }
}
