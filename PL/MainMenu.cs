﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BL;


namespace PL
{
    public class MainMenu : Display
    {
        public IBL aBL;
        public DoctorMenu docMenu;
        public PatientsMenu patMenu;
        public VisitsMenu visMenu;
        public TreatmentMenu treMenu;
        public Stack<Display> back;
        public Display display = null;//@ Stack to contain all user operations and can go back

        public MainMenu(IBL aBL)
        {
            this.aBL = aBL;
            docMenu = new DoctorMenu(this);
            patMenu = new PatientsMenu(this);
            visMenu = new VisitsMenu(this);
            treMenu = new TreatmentMenu(this);
            back = new Stack<Display>();
        }

       override public void displayMenu()
        {
           Console.WriteLine("Welcome To MaxStar");
           Console.WriteLine("==================");
           Console.WriteLine("Main Menu:");
           Console.WriteLine("----------");
           Console.WriteLine("1) Doctors");
           Console.WriteLine("2) Patients");
           Console.WriteLine("3) Visits");
           Console.WriteLine("4) Treatments");
           Console.WriteLine("5) Exit");
        }

       override public void ParseCmd(String cmd)
       {    
           switch(cmd)
           {
               case "1":
                   back.Push(this);
                   Console.Clear();
                   docMenu.displayMenu();
                   docMenu.ParseCmd(ReceiveCmd());
                   break;

               case "2":
                   back.Push(this);
                   Console.Clear();
                   patMenu.displayMenu();
                   patMenu.ParseCmd(ReceiveCmd());
                   break;

               case "3":
                   back.Push(this);
                   Console.Clear();
                   visMenu.displayMenu();
                   visMenu.ParseCmd(ReceiveCmd());
                   break;

               case "4":
                   back.Push(this);
                   Console.Clear();
                   treMenu.displayMenu();
                   treMenu.ParseCmd(ReceiveCmd());
                   break;

               case "5":
                   Console.WriteLine();
                   break;

               default:
                   Console.WriteLine("Invalid selection. Please select 1, 2, 3, 4 or 5.");
                   this.ParseCmd(ReceiveCmd());
                   break;
           }
       }
               
        public void goBack()
        // a function that display the previous menu
        {
            display = back.Pop();
            display.displayMenu();
            display.ParseCmd(ReceiveCmd());
        }

        public void returnToMainMenu()
        // a function that display main menu
        {
            displayMenu();
            ParseCmd(ReceiveCmd());
        }

    }
}
