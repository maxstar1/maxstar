﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;
using BL;

namespace PL
{
    public class DoctorProfileMenu : Display
    // Display Doctor profile
    {
        public MainMenu mainMenu;
        private Doctor doctor;
        public IBL aBL;

        public DoctorProfileMenu( MainMenu mainMenu, Doctor doctor)
        {
            aBL = mainMenu.aBL;
            this.mainMenu = mainMenu;
            this.doctor = doctor;
        }

        override public void displayMenu()
        {
            Console.WriteLine("Doctor Profile");
            Console.WriteLine("========================");
            Console.WriteLine("Name: Dr."+doctor.firstName+" "+doctor.lastName); 
            Console.WriteLine("Gender: "+doctor.gender);
            Console.WriteLine("Age: "+doctor.age);
            Console.WriteLine("Salary: "+doctor.salary);
            Console.WriteLine("------------------------");
            Console.WriteLine("Doctor Details");
            Console.WriteLine();
            Console.WriteLine("1) List of Patiens");
            Console.WriteLine("2) Edit Salary");
            Console.WriteLine("3) Back to Doctor Menu");
            Console.WriteLine("4) Back to Main Menu");
        }
        

        override public void ParseCmd(String cmd)
        {
            switch (cmd)
            {
                case "1": //@List of Patiens
                    mainMenu.back.Push(this);
                    Console.Clear();
                    List<Patient> p = aBL.getListOfPatientD(doctor);
                    if (p==null)
                    {
                        Console.WriteLine("List is empty");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.print(p.ConvertAll(i => i as Person));
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        mainMenu.goBack();
                    }
                    break;

                case "2": //@Edit salary
                    mainMenu.back.Push(this);
                    aBL.changeDetails("Doctor", doctor.id);
                    Console.WriteLine("Salary changed successfully press any key to refresh");
                    ReceiveCmd();
                    Console.Clear();
                    this.displayMenu();
                    this.ParseCmd(ReceiveCmd());
                    break;

                case "3": //@ go back
                    Console.Clear(); 
                    mainMenu.goBack();
                    break;

                case "4": //@ back to main menu
                    Console.Clear();
                    mainMenu.returnToMainMenu();
                    break;

                default:
                    Console.WriteLine("Invalid selection. Please select 1, 2, 3 or 4.");
                    this.ParseCmd(ReceiveCmd());
                    break;
            }
        }
    }
}
