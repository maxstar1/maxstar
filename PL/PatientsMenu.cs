﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL;
using BL_BackHand;

namespace PL
{
    public class PatientsMenu : Display
    {
        public MainMenu mainMenu;
        public IBL aBL;
        public Edit edit;
        public PatientsMenu(MainMenu mainMenu)
        {
            aBL = mainMenu.aBL;
            this.mainMenu = mainMenu;
        } 

        override public void displayMenu()
        {
            Console.WriteLine("Patients Menu");
            Console.WriteLine("-------------");
            Console.WriteLine("1) List of Patients by firstName");
            Console.WriteLine("2) List of Patients by LastName");
            Console.WriteLine("3) List of Patients by Age");
            Console.WriteLine("4) Show Patient profile");
            Console.WriteLine("5) Edit");
            Console.WriteLine("6) Back to Main Menu");
        }

        override public void ParseCmd(String cmd)
        {
            switch (cmd)
            {
                case "1":
                    mainMenu.back.Push(this);
                    Console.Clear();
                    if (aBL.getByName("Patient").Count() == 0)
                    {
                        Console.WriteLine("List is empty");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.print(aBL.getByName("Patient"));
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    break;

                case "2":
                    mainMenu.back.Push(this);
                    Console.Clear();
                    if (aBL.getByLname("Patient").Count() == 0)
                    {
                        Console.WriteLine("List is empty");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.print(aBL.getByLname("Patient"));
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    break;
                    
                case "3":
                    mainMenu.back.Push(this);
                    Console.Clear();
                    if (aBL.getByAgeP().Count==0)
                    {
                        Console.WriteLine("List is empty");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        printList.print(aBL.getByAgeP().ConvertAll(i => i as Person));
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    break;

                case "4":
                    mainMenu.back.Push(this);
                    Console.Clear();
                    Patient doc = aBL.FindByIdP();
                    if (doc == null)
                    {
                        Console.Clear();
                        Console.WriteLine("Patient not found");
                        Console.WriteLine("Press any key to go back");
                        ReceiveCmd();
                        Console.Clear();
                        mainMenu.goBack();
                    }
                    else
                    {
                        PatientProfileMenu profile = new PatientProfileMenu(mainMenu, doc);
                        profile.displayMenu();
                        profile.ParseCmd(ReceiveCmd());
                    }
                    break;

                case "5":
                    mainMenu.back.Push(this);
                    Console.Clear();
                    edit = new Edit("Patient", mainMenu);
                    edit.displayEditMenu();
                    break;

                case "6":
                    Console.Clear();
                    mainMenu.goBack();
                    break;

                default:
                    Console.WriteLine("Invalid selection. Please select 1, 2, 3, 4, 5 or 6.");
                    this.ParseCmd(ReceiveCmd());
                    break;
            }
        }
    }
}
