﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PL_GUI2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        private void Close(object sender, System.Windows.RoutedEventArgs e)
        {
        	this.Close();
        }

        private void Hide(object sender, System.Windows.RoutedEventArgs e)
        {
        	this.Hide();
        }
		
			private void ComboBox_Loaded(object sender, RoutedEventArgs e)
	{
	    // ... A List.
	    List<string> data = new List<string>();
        data.Add("Administrator");
	    data.Add("Doctor");
	    data.Add("Patient");

	    // ... Get the ComboBox reference.
	    var comboBox = sender as ComboBox;

	    // ... Assign the ItemsSource to the List.
	    comboBox.ItemsSource = data;
	}

			private void Login(object sender, System.Windows.RoutedEventArgs e)
			{
                DoctorProfile DocMain = new DoctorProfile();
    			App.Current.MainWindow = DocMain;
   				this.Close();
    			DocMain.Show();
			}
	
    }
}
