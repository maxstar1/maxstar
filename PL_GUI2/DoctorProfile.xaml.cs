﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PL_GUI2
{
    /// <summary>
    /// Interaction logic for DoctorProfile.xaml
    /// </summary>
    public partial class DoctorProfile : Window
    {
        public DoctorProfile()
        {
            InitializeComponent();
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        private void Close(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Close();
        }

        private void Hide(object sender, System.Windows.RoutedEventArgs e)
        {
            this.Hide();
        }

        private void Send_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
        }

        private void Logout_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	MainWindow Main = new MainWindow();
            App.Current.MainWindow = Main;
   			this.Close();
    		Main.Show();
        }
    }
}
