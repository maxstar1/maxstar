﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_BackHand
{
    public class Person
    {
        public Checker c = new Checker();
        public Person(string newId)
        {

            this.id = newId;
            firstName = Console.ReadLine();
            lastName = Console.ReadLine();
            gender = Console.ReadLine();
            age = Console.ReadLine();
        }

        public Person(String firstName, String lastName, String age, String id)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.age = age;
            this.gender = "male";
            this.id = id;
        }

        private string _id;
        public string id
        {
            get { return this._id; }
            set
            {
                try
                {
                    if (c.checkLength(value,9)==false)
                    {
                        throw new Exception("Id should be nine digits");
                    }
                    if(c.checkOnlyDigitis(value) == false)
                    {
                        throw new Exception("Id should contain only digits"); 
                    }
                    this._id = value;

                }
                catch (Exception e)// return to id sign
                {
                    id = Console.ReadLine();
                }

            }
        }
        private String _firstName;
        public String firstName
        {
         get { return _firstName; }
            set
            {
                try
                {
                    if (c.checkUppercase(value)==false)
                    {
                        throw new Exception("The first letter should be a uppercase");
                    }

                    if (c.checkOnlyLetters(value) == false)
                    {
                        throw new Exception("Name should contain only letters");
                    }
                    _firstName = value;
                }
                catch (Exception e)// return to id sign
                {
                    firstName = Console.ReadLine();
                }
            }
        }
        private String _lastName;
        public String lastName
        {
            get { return _lastName; }
            set
            {
                try
                {
                    if (c.checkUppercase(value) == false)
                    {
                        throw new Exception("The first letter should be a uppercase");
                    }

                    if (c.checkOnlyLetters(value) == false)
                    {
                        throw new Exception("Name should contain only letters");
                    }
                    _lastName = value;
                }
                catch (Exception e)// return to id sign
                {
                    Console.WriteLine(e);
                    lastName = Console.ReadLine();
                }
            }
        }
        private String _gender;
        public String gender
                {
            get { return _gender; }
            set
            {
                try
                {
                    if (value != "male" & value != "female")
                    {
                        throw new Exception("Gender can be 'male' or 'female'.");
                    }
                    _gender = value;
                }
                catch (Exception e)// return to id sign
                {
                    gender = Console.ReadLine();
                }
            }
        }
        private String _age;
        public String age
        {
            get { return _age; }
            set {
                try
                {
                    if ((value.Length < 0 & value.Length > 3) || (c.checkOnlyDigitis(value) == false))
                    {
                        throw new Exception("Age is not apropriate.");
                    }
                    else{_age = value;}
                }
                catch (Exception e)// return to age sign
                {
                    age = Console.ReadLine();
                }
            }
        }

      
    }
}
