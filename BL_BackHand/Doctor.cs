﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_BackHand
{
    public class Doctor : Person
    {
        private Checker c = new Checker();
        private List<Date> _dates;
        public List<Date> dates
        {
            get { return _dates; }
            set
            {   
                   _dates = value;
            }
        }
        private String _salary;
        public String salary
        {
            get { return _salary; }
            set
            {
                try
                {
                    if (value.Length < 0 || c.checkOnlyDigitis(value) == false )
                    {
                        throw new Exception("invalid salary- Re enter a valid salary:");
                    }
                    _salary = value;
                }
                catch (Exception e)
                {
                    salary = Console.ReadLine();
                }
            }
        }
        public Doctor(string newId)
            : base(newId)
            
        {
           salary = Console.ReadLine();
           
        }

        public Doctor(String firstName, String lastName, String age, String id, String salary) : base(firstName,lastName,age,id)
        {
            this.salary = salary;
            this._dates = null;

        }

    }

}
