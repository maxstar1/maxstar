﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_BackHand
{
    public class Visit
    {
            public Checker c = new Checker();
        private String _visitID;
        public String visitID
        {
            get { return _visitID;}
            set{
                try
                {
                    if (c.checkLength(value, 4) == false)
                    {
                        throw new Exception("Id length should be 4.");
                    }
                    if (c.checkOnlyDigitis(value) == false)
                    {
                        throw new Exception("Id should contain only digits");
                    }
                     _visitID = value;
                    
                }
                catch (Exception e)// return to id sign
                
                {
                    visitID = Console.ReadLine();
                }
            }
        }

        
        private String _dateOfVisit;
        public String dateOfVisit
        {
            get { return _dateOfVisit; }
            set
            {
                try
                {
                    if (c.checkDate(value) == true)
                    {
                         _dateOfVisit = value;
                    }
                       
                    else
                    {
                        throw new Exception("The date should be a valid date like 'DD.MM.YYYY'");
                    }

                }
                catch (Exception e)// return to id sign
                {
                    dateOfVisit = Console.ReadLine(); 
                }
            }
        }
        private Doctor _AssignedDoctor;
        public Doctor AssignedDoctor
        {
            get { return _AssignedDoctor; }
            set { _AssignedDoctor = value; }
        }
        private String _PatientID;
        public String PatientID
        {
            get { return _PatientID; }
            set { _PatientID = value; }

        }
        private String _DoctorNotes;
        public String DoctorNotes
        {
            get { return _DoctorNotes; }
            set { _DoctorNotes = value; }
        } 
        private String _TreatmentsMade;
        public String TreatmentsMade
        {
            get { return _TreatmentsMade; }
            set { _TreatmentsMade = value; }

        }
        public  Visit(Patient p, Doctor d, String id)
        {
            visitID = id;
            dateOfVisit = Console.ReadLine();
            AssignedDoctor=d;
            DoctorNotes = Console.ReadLine();
            TreatmentsMade = Console.ReadLine();
            PatientID = p.id;
        }
        
    }
}
