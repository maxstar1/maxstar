﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_BackHand
{
    public class Patient : Person
    {
        private Doctor _mainDoctor;
        public Doctor mainDoctor
        {
            get { return _mainDoctor; }
            set { _mainDoctor = value; }
        }
        private List<PatDate> _dates;
        public List<PatDate> dates
        {
            get { return _dates; }
            set{ _dates = value;}
        }

        public Patient(Doctor doc, String newId) : base(newId) 
        {
            mainDoctor = doc;
            _dates = null;
        }

        public Patient(String firstName, String lastName, String age, String id, Doctor doc)
            : base(firstName, lastName, age, id )
        {
            mainDoctor=doc;
            _dates = null;
        }


    }
}
