﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_BackHand
{
    public class Date
    {
        private string _day;
        public string day
        {
            get { return _day; }
            set { _day = value; }
        }
        public String[] time = { "09:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00" };
        private bool[] _isAvaileble = new bool[9];
        public bool[] isAvaileble
        {
            get { return _isAvaileble; }
            set { _isAvaileble = value; }
        }
        public void setAvale(int index, bool ave)
        {
            isAvaileble[index] = ave;
        }
        private Patient[] _pat = new Patient[9];
        public Patient[] pat
        {
            get { return _pat; }
            set { _pat = value; }
        }
        public void setPat(Patient pati, int index)
        {
            pat[index] = pati;
        }
        private void Date()
        {
            for (int i = 0; i < 9; i++)
            {
                setPat(null, i);
                setAvale(i, true);
            }
            day = null;
        }
        public Date(string day, Patient pat, int index)
        {
            Date();
            this.day = day;
            isAvaileble[index] = false;
            this.pat[index] = pat;
        }
        public Date(string day)
        {
            Date();
            this.day = day;

        }
        public Date(String day)
        {
            Date();
            this.day = day;
            
        }

    }
}
