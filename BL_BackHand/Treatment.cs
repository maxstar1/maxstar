﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_BackHand
{
    public  class Treatment
    {
        private Checker c = new Checker();
        private String _DateOfStart;
        public String DateOfStart
        {
        get {return _DateOfStart;}
            set
            {
                try
                {
                    if (c.checkDate(value) == false)
                    {
                        throw new Exception("The date should be a valid date like 'DD.MM.YYYY'");
                    }
                    _DateOfStart = value;
                }
                catch (Exception e)
                {
                    DateOfStart = Console.ReadLine();
                }
            }
        }
        private String _DateOfFinish;
        public String DateOfFinish
        {
            get { return _DateOfStart; }
            set
            {
                try
                {
                    if (c.checkDate(value) == false)
                    {
                        throw new Exception("The date should be a valid date like 'DD.MM.YYYY'");
                    }
                    _DateOfFinish = value;
                }
                catch (Exception e)
                {
                    DateOfFinish = Console.ReadLine();
                }
            }
        }
        private Doctor _createdByDoctor;
        public Doctor createdByDoctor
        {
            get { return _createdByDoctor; }
            set { _createdByDoctor = value; }
        }
        private String _Prognosis;
        public String Prognosis
        {
            get { return _Prognosis; }
            set { _Prognosis = value; }
        }
        private String _Prescriptions;
        public String Prescriptions
        {
            get { return _Prescriptions; }
            set { _Prescriptions = value;}
        }
        public Patient pati;

        private String _treatmentID;
        public String treatmentID
        {
            get { return _treatmentID; }
            set
            {
                try
                {
                    if (c.checkLength(value, 4) == false)
                    {
                        throw new Exception("Id length should be 4.");
                    }
                    if (c.checkOnlyDigitis(value) == false)
                    {
                        throw new Exception("Id should contain only digits");
                    }
                    _treatmentID = value;

                }
                catch (Exception e)// return to id sign
                {
                    treatmentID = Console.ReadLine();
                }
            }
        }

        public Treatment(Doctor doc , Patient pat, String id )
        {
            pati = pat;
            treatmentID = id;
            createdByDoctor = doc;
            DateOfStart = Console.ReadLine();
            DateOfFinish = Console.ReadLine();
            Prognosis = Console.ReadLine();
            Prescriptions = Console.ReadLine();

        }
    }
}
