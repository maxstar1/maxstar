﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL_BackHand
{
    public class PatDate
    {
        private String _hour;
        public String hour
        {
            get
            {
                return _hour;
            }
            set
            {
                this._hour = value;
            }
        }

        private String _date;
        public String date
        {
            get
            {
                return _date;
            }
            set
            {
                this._date = value;
            }
        }
        public PatDate(String date , String hour)
        {
            _hour = hour;
            _date = date;
        }

    }
}
