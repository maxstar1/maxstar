﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BL_BackHand;

namespace BL
{
    public class BL_V 
    {
        private Checker c;
        private MasterBL masterBL;
        private IDAL dal;
        public BL_V(MasterBL masterBL)
        {
            c = new Checker();
            this.masterBL = masterBL;
            dal = masterBL.dal;
        }
        public Visit getSpecificById()
        {
            Console.WriteLine("Please Enter the Id of the visit you want to find:");
            String id = Console.ReadLine();
            if (c.checkOnlyDigitis(id) == true && c.checkLength(id, 4) == true)
            {
                bool isExsist = dal.ExistId(id, "Visit");
                if (isExsist == false)
                {
                    return dal.getSpecificByIdV(id);
                }
                return null;
                
            }
            else
            {
                Console.WriteLine("Invalid id, should be 4 digits");
                return null;
            }
        }
        public List<Visit> visitsByDateV()
        {
            List<Visit> vis = dal.visitsByDateV();
            return vis;
        }
        public void add()
        {
            Console.WriteLine("Please Enter the Id of the visit :");
            String id = Console.ReadLine();
            if (c.checkOnlyDigitis(id) == true && c.checkLength(id, 4) == true)
            {
                bool isExsist = dal.ExistId(id, "Visit");
                if (isExsist == false)
                {
                    Doctor doc = masterBL.doc.FindByIdD();
                    Patient pat = masterBL.pat.FindByIdP();
                    Visit vis = new Visit(pat, doc, id);
                    dal.addIT(vis);
                    Console.WriteLine("A visit has been succsessfuly added.");
                    return;

                }
                Console.WriteLine("Id already exist, please re-enter a valid id:");
                add();
            }
            else
            {
                Console.WriteLine("Invalid id, should be 4 digits");
                add();
            }
        }


        public object viz { get; set; }
    }
}
