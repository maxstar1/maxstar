﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BL_BackHand;

namespace BL
{
    public class BL_T 
    {
        private IDAL dal;
        private MasterBL masterBL;
        private Checker c;

        public BL_T(MasterBL masterBL)
        {
            c = new Checker();
            this.masterBL = masterBL;
            dal = masterBL.dal;
        }
        public void add()
        {
            Console.WriteLine("Please Enter the Id of the treatment:");
            String id = Console.ReadLine();
            if (c.checkOnlyDigitis(id) == true && c.checkLength(id, 4) == true)
            {
                bool isExsist = dal.ExistId(id, "Treatment");
                if (isExsist == false)
                {
                    Doctor doc = masterBL.doc.FindByIdD();
                    Patient pat = masterBL.pat.FindByIdP();
                    Treatment tre = new Treatment(doc, pat, id);
                    dal.addIT(tre);
                    return;
                }
                
                Console.WriteLine("Id already exist, please re-enter a valid id:");
                add();
            }
            else
            {
                Console.WriteLine("Invalid id, should be 4 digits");
                add();
            }
        }
        public List<Treatment> treatmentByDateT() 
        {
            List<Treatment> tre = dal.treatmentByDateT();
            return tre;
        }
        public Treatment getSpecificById()
        {
            Console.WriteLine("Please Enter the Id of the treatment you want to find:");
            String id = Console.ReadLine();
            if (c.checkOnlyDigitis(id) == true && c.checkLength(id, 4) == true)
            {
                bool isExsist = dal.ExistId(id, "Treatment");
                if (isExsist == false)
                {
                    return dal.getSpecificByIdT(id);
                }
                return null;
            }
            else
            {
                Console.WriteLine("Invalid id, should be 4 digits");
                return null;
            }
        }
    }
}
