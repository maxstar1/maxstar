﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;
using DAL;

namespace BL
{
    public class BL_D 
    {
        private IDAL dal;
        private MasterBL masterBL;
        public BL_D(MasterBL masterBL)
        {
            this.masterBL = masterBL;
            dal = masterBL.dal;
        }
        public void add()
        {
            Console.WriteLine("Enter id:");
            String id = Console.ReadLine();
            if (dal.ExistId(id, "Doctor") == false)
            {
                Doctor doc = new Doctor(id);
                dal.addIT(doc);
                Console.WriteLine("A doctor has been succsessfuly added.");
                return;
            }
            else
            {
                Console.WriteLine("This Id alredy exist, please enter an other Id:");
                add();
            }
        }
        public bool changeDetails(String name, String id ) 
        {

            Doctor doc = dal.FindByIdD(id);
            if (doc == null) { return false; }
            Console.Write("Please enter amount:");
            String sal = Console.ReadLine();
            dal.editSalaryD(sal, doc);
            return true;

        }
        public bool removeById() 
        {
            Doctor doc = FindByIdD();
            if (doc == null) { return false; }
            dal.removeD(doc);
            return true;
        }
        public Doctor FindByIdD() 
        {
            Console.WriteLine("Pleas Enter the Id of the Doctor you want to find:");
            String id = Console.ReadLine();
            bool isExsist = dal.ExistId(id, "Doctor");
            if (isExsist == false)
            {
                return null;
            }
             return dal.FindByIdD(id); 
        }
        public List<Doctor> getBySalary()
        {
            return dal.getBySalaryD();
        }
        public List<Person> getByName(String type)
        {
            List<Person> name = dal.GetOrderListNameP("Doctor"); 
            return name;
        }
        public List<Person> getByLname(String type)
        {
            List<Person> name = dal.GetOrderListLname("Doctor");
            return (name);
        }
        public List<Patient> getListOfPatientD(Doctor d) 
        {
            List<Patient> ans = dal.getListOfPatientD(d);
            if (ans.Count > 0) { return ans; }
            return null;

        }

    }
}
