﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BL_BackHand;

namespace BL  
{
    public class MasterBL : IBL
    {
        public BL_D doc;
        public BL_P pat;
        public BL_V vis;
        public BL_T tre;
        public IDAL dal;
        public MasterBL(IDAL d)
        {
            dal = d;
            doc = new BL_D(this);
            vis = new BL_V(this);
            tre = new BL_T(this);
            pat = new BL_P(this);
        }
        public void add(String type)
        {
            switch (type)
            {
                case "Doctor":
                    doc.add();
                break;
                case "Patient":
                pat.add();
                break;
                case "Visit":
                vis.add();
                break;
                case "Treatment":
                tre.add();
                break;
            }
        }
        public List<Person> getByName(String type)
        {
            if (type == "Doctor") { return doc.getByName(type); }
            else { return pat.getByName(type); }
            
        }
        public List<Person> getByLname(String type)
        {
            if (type == "Doctor") { return doc.getByLname(type); }
            else { return pat.getByLname(type); }
        }
        public bool changeDetails(String type, String id)
        {
            if (type == "Doctor") { return doc.changeDetails(type, id); }
            else { return pat.changeDetails(type, id); }
        }
        public bool removeById(String type)
        {
            if (type == "Doctor") { return doc.removeById(); }
            else { return pat.removeById(); }
        }
        public Doctor FindById()
        {
            return doc.FindByIdD();
        }
        public List<Patient> getListOfPatientD(Doctor d)
        {
            return doc.getListOfPatientD(d);
        }
        public List<Doctor> getBySalary()
        {
            return doc.getBySalary();
        }
        public List<Patient> getByAgeP()
        {
            return pat.getByAgeP();
        }
        public List<Treatment> getTreatmentP(Patient p)
        {
            return pat.getTreatmentP(p);
        }
        public List<Visit> getVisitsP(Patient pat)
        {
            return this.pat.getVisitsP(pat);
        }
        public Patient FindByIdP()
        {
            return pat.FindByIdP();
        }
        public Visit getSpecificByIdV()
        {
            return vis.getSpecificById();
        }
        public List<Visit> visitsByDateV()
        {
            return vis.visitsByDateV();
        }
        public List<Treatment> treatmentByDateT()
        {
            return tre.treatmentByDateT();
        }
        public Treatment getSpecificByIdT()
        {
            return tre.getSpecificById();
        }
    }
}
