﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;


namespace BL
{
    public interface IBL
    { 
        //general metuods
        void add(String type);// for all
        List<Person> getByName(String type);// for patient and doctor
        List<Person> getByLname(String type);// for patient and doctor
        bool changeDetails(String type, String id);// for patient and doctor
        bool removeById(String type);// for patient and doctor
        bool addNewUser(String type, String id, String userName, String pass);
        bool additPass(string type, string userName, string pass);
        

        //doctor methods!
        
        Doctor FindById();
        List<Patient> getListOfPatientD(Doctor d);
        List<Doctor> getBySalary();
        Doctor loginD(string type, string userName, string pass);

        //Patient 
        List<Patient> getByAgeP();
        List<Treatment> getTreatmentP(Patient p);
        List<Visit> getVisitsP(Patient pat);
        Patient FindByIdP();
        Patient loginP(string type, string userName, string pass);
        List<PatDate> getNextVisits(string id);

        //visit
        Visit getSpecificByIdV();
        List<Visit> visitsByDateV();
        Visit addit(string id, string DoctorNotes, string TreatmentsMade);

        //treatment
        List<Treatment> treatmentByDateT();
        Treatment getSpecificByIdT();
        Treatment addit(string id, string DateOfFinish, string Prescriptions, string Prognosis);
    }
   

}
