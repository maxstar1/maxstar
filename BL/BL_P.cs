﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using BL_BackHand;


namespace BL
{
    public class BL_P 
    {
        private Checker c = new Checker();
        private MasterBL masterBL;
        private IDAL dal;
        public BL_P(MasterBL masterBL)
        {
            // TODO: Complete member initialization
            this.masterBL = masterBL;
            dal = masterBL.dal;
        }
        public void add()
        {
            Doctor doc = dal.getRandomDoc();
                    if (doc == null)
                    {
                        Console.WriteLine("Pattient can't be acceppted, there are no doctors in the HMO.");
                        return;
                    }
            Console.WriteLine("Enter id:");
            String id = Console.ReadLine();
            if (c.checkOnlyDigitis(id) == true && c.checkLength(id, 9) == true)
            {
                if (dal.ExistId(id, "Patient") == false)
                {

                    Console.WriteLine("The doctor assingd to you is: dr' " + doc.firstName + " " + doc.lastName + " " + doc.id + " .");
                    Patient pat = new Patient(doc, id);
                    dal.addIT(pat);
                    Console.WriteLine("A patient has been succsessfuly added.");
                    return;
                }


                else
                {
                    Console.WriteLine("This Id alredy exist, please enter an other Id:");
                    add();
                }
            }
            else
            {
                Console.WriteLine("Invalid id, should be 9 digits");
                add();
            }
        }
        public bool changeDetails(String name, String id)
        {
            Patient pat = dal.FindByIdP(id);
            Doctor doc =masterBL.doc.FindByIdD();
            if (doc == null) { return false; }
            pat.mainDoctor = doc;
            return true;

        }
        public bool removeById()
        {
            Patient pat = FindByIdP();
            if (pat == null) { return false; }
            dal.removeP(pat);
            return true;
        }
        public Patient FindByIdP()
        {
            Console.WriteLine("Pleas Enter the Id of the Patient you want to find:");
            String id = Console.ReadLine();
            Console.Clear();
            bool isExsist = dal.ExistId(id, "Patient");
            if (isExsist == false)
            {
                return null;
            }
            return dal.FindByIdP(id);
        }
        public List<Patient> getByAgeP() 
        {
            List<Person> age = dal.getByAgeP();
            if (age == null) { return null; }
            List<Patient> ans = age.ConvertAll(x => x as Patient);
            return ans;
        }
        public List<Visit> getVisitsP(Patient pat) 
        {
            List<Visit> ans = dal.getVisitsP(pat);
            if (ans == null) { return null; }
            return ans;
        }
        public List<Treatment> getTreatmentP(Patient p) 
        {
            List<Treatment> ans = dal.getTreatmentP(p);
            if (ans == null) { return null; }
            return ans;
        }
        
        public List<Person> getByName(String type)
        {
            List<Person> name = dal.GetOrderListNameP("Patient");
            return (name);
        }
        public List<Person> getByLname(String type)
        {
            List<Person> name = dal.GetOrderListLname("Patient");
            return (name);
        }

    }
}