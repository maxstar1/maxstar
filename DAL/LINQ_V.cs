﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;

namespace DAL
{
    public class LINQ_V
    {
        Master_DAL master;
        List<Visit> DBVisit;
        public LINQ_V(Master_DAL m)
        {
            DBVisit = new List<Visit>();
            master =m;

        }
        public List<Visit> allVis() 
        
        {
            List<Visit> copy = new List<Visit>(DBVisit);
            return copy;
        }
    
        public List<Visit> getVisits(Patient pat)
        {
            List<Visit> copy = new List<Visit>(DBVisit);
            List<Visit> ans = new List<Visit>();
            String id = pat.id;
            if (copy != null)
            {
                foreach (Visit visi in copy)
                {
                    if (visi.visitID == id) // Will match once
                    {
                        ans.Add(visi);
                    }
                }
            }


            return ans;


        }
        public List<Visit> visitsByDate()
        {
            List<Visit> copy = new List<Visit>(DBVisit);
            if (copy.Count > 0)
            {
                List<Visit> SortedList = copy.OrderBy(o => o.dateOfVisit).ToList();
                return SortedList;
            }
            return copy;
        }
        public void AddIt(Visit v)
        {
            DBVisit.Add(v);
        }
        public void remove(Patient p)
        {
            List<Visit> remove = new List<Visit>();
            if (DBVisit.Count > 0)
            {
            foreach (Visit ver in DBVisit)
            {
                if (ver.PatientID != p.id)
                {
                    remove.Add(ver);
                }
            
            }
                this.DBVisit= remove;
            }
        }
        public Visit getSpecificById(string id)
        {

            if (DBVisit.Count() > 0)
            {
                foreach (Visit vis in DBVisit)
                {
                    if (vis.visitID == id)
                    {
                        return vis;
                    }
                }
            }
            return null;

        }
        public Date getAVhours(Patient pat, String date)
        {
            Doctor dre = pat.mainDoctor;
            List<Date> temp = dre.dates;
            Date ans= new Date(date);
            foreach (Date exsistDate in temp)
            {
                if (exsistDate.day == date)
                    ans = exsistDate;
            }
            return ans;
      
        }
        public void slectedHour(Patient pat, int hourIndex, String date)
        {
            Doctor dre = pat.mainDoctor;
            Date newDate = new Date(date);
            foreach (Date exsist in dre.dates)
            {
                if (exsist.day == date)
                    newDate = exsist;
            }
            newDate.isAvaileble[hourIndex] = false;
            newDate.pat[hourIndex] = pat;
            //change the "Date" in the doctor's list
            foreach (Date exsist in dre.dates)
            {
                if (exsist.day == date)
                {
                    exsist.pat = newDate.pat;
                    exsist.isAvaileble = newDate.isAvaileble;
                }
            }
            //add the new visit to the patient's list
            String hour = newDate.time[hourIndex];
            List<PatDate> ans = pat.dates;
            PatDate newPatDate = new PatDate(date, hour);
            ans.Add(newPatDate);
            pat.dates = ans;
        }

    }
    
}
