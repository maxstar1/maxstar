﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;

namespace DAL
{
    public class Master_DAL : IDAL
    {
        public LINQ_D doc;
        public LINQ_P pat;
        public LINQ_T tre;
        public LINQ_V vis;
        public DDPL docPat;
        public Master_DAL()
        {
            doc = new LINQ_D(this);
            pat = new LINQ_P(this);
            tre = new LINQ_T(this);
            vis = new LINQ_V(this);

            Doctor doc1 = new Doctor("Robin", "Vanpersi", "28", "123456789", "50000");
            Doctor doc2 = new Doctor("Leo", "Messi", "27", "987654321", "207000");
            Doctor doc3 = new Doctor("Wyane", "Ronny", "29", "000000000", "15");
            Doctor doc4 = new Doctor("Alvaro", "Mortta", "39", "111111111", "38999");

            doc.AddIt(doc1);
            doc.AddIt(doc2);
            doc.AddIt(doc3);
            doc.AddIt(doc4);

            Patient p1 = new Patient("Carlos", "Garcia", "46", "777777777", doc1);
            Patient p2 = new Patient("Miki", "Gonzalz", "19", "333333333", doc1);
            Patient p3 = new Patient("Toto", "Tamuz", "3", "444444444",doc2);
            Patient p4 = new Patient("Itay", "Shecter", "25", "555555555",doc2);
            Patient p5 = new Patient("Avi", "Nimni", "49", "666666666", doc3);

            pat.AddIt(p1);
            pat.AddIt(p2);
            pat.AddIt(p3);
            pat.AddIt(p4);
            pat.AddIt(p5);
        }

        //patient

        //return list of patients.
        public List<Person> getListP()
        {

            return pat.getList();
        }
        // find patient by given id
        public Patient FindByIdP(String id)
        {
            return pat.FindById(id);
        }
        public List<Person> getByAgeP()
        {
            return pat.getByAge();
        }
        public List<Visit> getVisitsP(Patient pat)
        {
            return vis.getVisits(pat);
        }
        public List<Treatment> getTreatmentP(Patient p)
        {

            return pat.getTreatment(p);
        }
        public void removeP(Patient p)
        {
            pat.remove(p);
        }

        //new
        public List<PatDate> AllNextVisitsP(Patient pati)
        {
            return pat.AllNextVisits(pati);
        }

        public void removeVisP(String day, Patient pati, int index)
        {
            pat.removeVis(day, pati, index);
        }

  /*      public void changePas(Patient pat, string pas)
        {
            return pat.changePas(pat, pas);
        }
        */

        //doctor

        public Doctor FindByIdD(String id)
        {
            return doc.FindById(id);
        }
        public List<Doctor> getBySalaryD()
        {
            return doc.getBySalary();
        }
        public void editSalaryD(String sal, Doctor d)
        {
            doc.editSalary(sal, d);
        }
        public List<Patient> getListOfPatientD(Doctor d)
        {
            return doc.getListOfPatient(d);
        }
        public void removeD(Doctor d)
        {
            doc.remove(d);
        }
        public Doctor getRandomDoc()
        {
            return doc.getRandomDoc();
        }

        //new 
        public Treatment changeTreatmentT(String id, String dateOfFinish, String prognosis, String prescriptions)
        {
            return doc.changeTreatment(id, dateOfFinish, prognosis, prescriptions);
        }
        public Visit changeVisitD(String id, String doctorNotes, String treatmentMade)
        {
            return doc.changeVisitD(id, doctorNotes, treatmentMade);
        }

        //visit

        public List<Visit> allVis()
        {

            return vis.allVis();
        }
        public List<Visit> visitsByDateV()
        {
            return vis.visitsByDate();
        }
        public void removeV(Patient p)
        {
            vis.remove(p);
        }
        public Visit getSpecificByIdV(string id)
        {
            return vis.getSpecificById(id);
        }

        //new
        public Date getAVhoursV(Patient pat, String date)
        {
            return vis.getAVhours(pat,date);
        }
        public void slectedHourV(Patient pat, int hourIndex, String date)
        {
            vis.slectedHour(pat, hourIndex, date);
        }


        //Treatment
        public List<Treatment> treatmentByDateT()
        {
            return tre.treaByDate();
        }
        public void removeT(Patient p)
        {
            tre.remove(p);
        }
        public Treatment getSpecificByIdT(string id)
        {
            return tre.getSpecificById(id);
        }

        // for all
        public void addIT(Object O)
        {
            if (O is Patient)
            {
                Patient p = O as Patient;
                pat.AddIt(p);
            }
            if (O is Doctor)
            {
                Doctor d = O as Doctor;
                doc.AddIt(d);
            }
            if (O is Visit)
            {
                Visit v = O as Visit;
                vis.AddIt(v);
            }
            if (O is Treatment)
            {
                Treatment t = O as Treatment;
                tre.AddIt(t);
            }

        }
        // doctor + patient
        public List<Person> GetOrderListNameP(String instn)
        {
            if (instn == "Patient")
            {
                return pat.GetOrderListName();
            }
            return doc.GetOrderListName();
        }
        public List<Person> GetOrderListLname(String instn)
        {
            if (instn == "Patient")
            {
                return pat.GetOrderListLname();
            }
            return doc.GetOrderListLname();
        }
        public bool ExistId(String id, String instn)
        {
            if (instn == "Patient")
            {
                return pat.ExsistId(id);
            }
            return doc.ExsistId(id);
        }
        public void newRegister(string pas, string id, string category, string userName)
        {
            if(category.Equals("Patient") )
            {
                Patient tempPat = this.pat.FindById(id);
                this.pat.newRegister(tempPat, pas, userName);
            }
            if (category.Equals("Doctor"))
            {
                Doctor tempDoc = this.doc.FindById(id);
                this.doc.newRegister(tempDoc, pas, userName);
            }
            
        }
        public Boolean userNameExsist(string newuser, string category)
        {
            if (category.Equals("Patient"))
            {
                return pat.userNameExsist(newuser);
            }
            if (category.Equals("Doctor"))
            {
                return doc.userNameExsist(newuser);
            }
            return false;
        }
        public void changePas(Person per, string pas)
        {
            if (per is Patient)
            {
                pat.changePas(per, pas);
            }
            if (per is Doctor)
            {
                doc.changePas(per, pas);
            }

        }
        // return doctor/patient/administrator by given pas+userName
        public Person getByPasAndUserName(string userName, string pass, string category)
        {
            if(category == "Doctor")
            {
                string id = doc.getByPasAndUserName(userName,  pass);
                return doc.FindById(id);
            }
            if (category == "Patient")
            {
                string id = pat.getByPasAndUserName(userName, pass);
                return pat.FindById(id);
            }
            if (category == "Administrator")
            {


            }
            //nevr hapend
            return null;
        }
    
    }
}





