﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;


namespace DAL
{
    
    public class LINQ_D : DDPL
    { 
        public Master_DAL master;
        public List<User> UserLink;
        public List<Person> DBDoctor;
        public LINQ_D(Master_DAL m)
        {
            DBDoctor = new List<Person>();
            master = m;
        }
        public void AddIt(Doctor p)
        {
            DBDoctor = base.AddIt(p, DBDoctor);
        }
        public bool ExsistId(String id)
        {
            bool ans = base.ExsistId(DBDoctor, id);
           
            return ans;
        }
        public Doctor FindById(String idd)
        {
            Doctor d = base.FindById(DBDoctor, idd) as Doctor;
            return d;
        }
        public List<Doctor> getBySalary()
        {
            
            List<Person> copy = new List<Person>(DBDoctor);
            List<Doctor> ans = new List<Doctor>();
           
            if (DBDoctor != null)
            {
                IEnumerable<Doctor> doc = copy.Cast<Doctor>();
                foreach (Doctor se in doc)
                {
                    ans = doc.OrderBy(o => o.salary).ToList();

                }
            }
            return ans;
         
        }
        public List<Person> GetOrderListLname()
        {
            List<Person> copy = new List<Person>(DBDoctor);
            List<Person> pat = (GetOrderListLname(copy));
            return pat;
        }
        public List<Person> getByAge()
        {
            List<Person> copy = new List<Person>(DBDoctor);
            List<Person> p = base.getByAge(copy);
            return p;
        }
        public List<Person> GetOrderListName()
        {
            List<Person> copy = new List<Person>(DBDoctor);
            List<Person> Pat = (base.GetOrderListName(copy));
            return Pat;
        }

        public void editSalary(String sal, Doctor d)
        {
            d.salary = sal;
        }
        public List<Patient> getListOfPatient(Doctor d)
        {
            List<Person> data = master.getListP();
            List<Patient> ans = new List<Patient>();
            String idd = d.id;
            foreach (Patient dat in data)
            {
                if (dat.mainDoctor.id == idd)
                {
                    ans.Add(dat);
                }
            }


            return ans;
        }
        public void remove(Doctor d)
        {
            if ( DBDoctor.Count > 0)
            {
                List<Patient> pat = new List<Patient>();
                List<Person> temp = master.getListP();
                String idr = d.id;
                int num = DBDoctor.Count();
                Random random = new Random();
                int index = random.Next(0, num);
                
                Doctor newDoc = DBDoctor[index] as Doctor;
                if (newDoc.id == d.id)
                    newDoc = DBDoctor.Last() as Doctor;
                if (newDoc.id == d.id)
                    newDoc = DBDoctor.First() as Doctor;


                foreach (Patient patientReplase in temp)
                {
                    if (patientReplase.mainDoctor.id == idr)
                    {
                        patientReplase.mainDoctor = newDoc;
                    }

                }
                
            }
            DBDoctor.Remove(d);
        }
        public Doctor getRandomDoc()
        {
            try 
            {
                int num = DBDoctor.Count();
                Random random = new Random();
                int index = random.Next(0, num);
                Doctor doc = DBDoctor[index] as Doctor;
                return doc;
            }
            catch(ArgumentOutOfRangeException e) 
            {
                return null;
            }
            /* if( DBDoctor.Count > 0)
            {

                int num = DBDoctor.Count();
                Random random = new Random();
                int index = random.Next(0, num);
                Doctor doc = DBDoctor[index] as Doctor;
                return doc;

            }        
        return null;*/

        }

        public Treatment changeTreatment(String id, String dateOfFinish, String prognosis, String prescriptions)
        {
            Treatment setTreat = master.getSpecificByIdT(id);
            setTreat.DateOfFinish = dateOfFinish;
            setTreat.Prognosis = prognosis;
            setTreat.Prescriptions = prescriptions;
            return setTreat;
        }
        
        public Visit changeVisitD(String id, String doctorNotes, String treatmentMade)
        {
            Visit ans = master.getSpecificByIdV(id);
            ans.DoctorNotes = doctorNotes;
            ans.TreatmentsMade = treatmentMade;
            return ans;
        }
        // adding the new Schedule of the doctor
        public void addVisitByDate(Date date, Doctor d)
        {
            List<Date> ans = d.dates;
            int count = 0;
            foreach (Date exsistDate in ans)
            {
                if (date.day == exsistDate.day)
                {
                    exsistDate.isAvaileble = date.isAvaileble;
                    exsistDate.pat = date.pat;
                    count++;
                }

            }
            if (count > 0)
                ans.Add(date);
        }
        public void newRegister(Doctor dre, string pas, string userName)
        {
            User newUs = new User(dre.id, userName, pas);
            UserLink.Add(newUs);

        }

        public Boolean userNameExsist(string newuser)
        {

            Boolean ans = false;



            // See if List contains the given id
            foreach (User member in UserLink)
            {
                if (member.userName == newuser) // Will match once
                {
                    ans = true;
                }
            }


            return false;
        }

        public void changePas(Person per, string pas)
        {
            string id = per.id;
            foreach (User member in UserLink)
            {
                if (member.id == id) // Will match once
                {
                    member.id = id;
                }
            }
        }
        //return the id of given user by user name+pass
        public string getByPasAndUserName(string userName, string pass)
        {

            string id = "";
            foreach (User member in UserLink)
            {
                if (member.userName == userName) // Will match once
                {
                    id = member.id;
                }
            }

            return id;
        }


    }
    

}
