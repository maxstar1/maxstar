﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;

namespace DAL
{
    public  class DDPL
    {
        
        public List<Person> AddIt(Person p, List<Person> o)
        {
            o.Add(p);
            return o;
        }
        public List<Person> GetOrderListName(List<Person> p)
        {
            List<Person> SortedList = p.OrderBy(o => o.firstName).ToList();
            return SortedList;
        }
        public List<Person> GetOrderListLname(List<Person> p)
        {
            List<Person> SortedList = p.OrderBy(o => o.lastName).ToList();
            return SortedList;
        }
        public List<Person> getByAge(List<Person> p)
        {
            List<Person> SortedList = p.OrderBy(o => o.age).ToList();
            return SortedList;
        }
        public Person FindById(List<Person> p, String idd)
        {

            List<Person> per = p;

            // See if List contains the given id
            foreach (Person member in per)
            {
                if (member.id == idd) // Will match once
                {
                    return member;
                }
            }

            return null;
        }
        public bool ExsistId(List<Person> p, String id)
        {
            bool ans = false;
            List<Person> per = p;
            foreach (Person pat in per)
            {
                if (pat.id == id) // Will match once
                {
                    ans = true;
                }
            }

            return ans;
        }
        

    }
}
