﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;


namespace DAL
{
    public class LINQ_P : DDPL
    {
        public Master_DAL master;
        public List<Person> DBPatient;
        public List<User> UserLink;
        public LINQ_P(Master_DAL m)
        {
            DBPatient = new List<Person>();
            master = m;

        }
        public List<Person> getList()
        {
            List<Person> copy = new List<Person>(DBPatient);
            return copy;
        }
        public void AddIt(Patient p)
        {
            DBPatient = base.AddIt(p, DBPatient);
        }
        public Patient FindById(String idd)
        {
            Patient p = base.FindById(DBPatient, idd) as Patient;
            return p;
        }
        public bool ExsistId(String id)
        {
            bool ans = base.ExsistId(DBPatient, id);
            return ans;
        }
        //sort the patients by name
        public List<Person> GetOrderListName()
        {
            List<Person> copy = new List<Person>(DBPatient);
            List<Person> Pat = (base.GetOrderListName(copy));
            return Pat;
        }
        //sort the patients by lastname
        public List<Person> GetOrderListLname()
        {
            List<Person> copy = new List<Person>(DBPatient);
            List<Person> pat = (GetOrderListLname(copy));
            return pat;
        }
        public List<Person> getByAge()
        {
            List<Person> copy = new List<Person>(DBPatient);
            List<Person> p = base.getByAge(copy);
            return p;
        }
        public List<Treatment> getTreatment(Patient pat)
        {
            List<Treatment> tri = new List<Treatment>();
            tri = master.treatmentByDateT();
            List<Treatment> ans= new List<Treatment>();
            String id = pat.id;
            if (tri.Count > 0)
            {
                foreach (Treatment titmen in tri)
                {
                    if (titmen.pati.id == id) // Will match once
                    {
                        ans.Add(titmen);
                    }
                }
            }


            return ans;

        }
        public void remove(Patient p)
        {
            master.removeT(p);
            master.removeV(p);
            DBPatient.Remove(p);

            
        }
        public List<PatDate> AllNextVisits(Patient pat)
        {
            return pat.dates;
        }
        public void removeVis(String day, Patient pat, int index)
        {
            Doctor dre = pat.mainDoctor;
            List<Date> dreDate = dre.dates;
            String patHour = "";
            foreach (Date exsist in dreDate)
            {
                if(exsist.day == day)
                {
                    exsist.isAvaileble[index]= true;
                    exsist.pat[index]= null;
                    patHour = exsist.time[index];
                }
            }

            List<PatDate> ans = pat.dates;
            foreach(PatDate exsis in ans)
            {
                if(exsis.date == day&&  patHour == exsis.hour)
                {
                    ans.Remove(exsis);
                }
            }
            pat.dates = ans;
        }
        public void newRegister(Patient pat, string pas, string userName)
        {
            User newUs = new User(pat.id, userName, pas);
            UserLink.Add(newUs);

        }
        public Boolean userNameExsist(string newuser)
        {

            Boolean ans = false;

            

            // See if List contains the given id
            foreach (User member in UserLink)
            {
                if (member.userName == newuser) // Will match once
                {
                    ans = true;
                }
            }


            return false;
        }
        public void changePas(Person per, string pas)
        {
            string id = per.id;
            foreach (User member in UserLink)
            {
                if (member.id == id) // Will match once
                {
                    member.id = id;
                }
            }
        }
        //return the id of given user by user name+pass
        public string getByPasAndUserName(string userName, string pass)
        {

            string id = "";
            foreach (User member in UserLink)
            {
                if (member.userName == userName) // Will match once
                {
                    id = member.id;
                }
            }

            return id;
        }

    }
}
