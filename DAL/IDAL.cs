﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;


namespace DAL
{
    public interface IDAL
    {

        // find patient by given id
        Patient FindByIdP(String id);
        List<Person> getByAgeP();
        List<Visit> getVisitsP(Patient pat);
        List<Treatment> getTreatmentP(Patient p);
        void removeP(Patient p);
        List<PatDate> AllNextVisitsP(Patient pat);

        //new functions
        void changePasP(Patient pat, string pas);
        void removeVisP(String day, Patient pat, int index);


        //doctor

        Doctor FindByIdD(String id);
        List<Doctor> getBySalaryD();
        void editSalaryD(String sal, Doctor d);
        List<Patient> getListOfPatientD(Doctor d);
        void removeD(Doctor d);
        //return random doctor
        Doctor getRandomDoc();
        Treatment changeTreatmentD(String id, String dateOfFinish, String prognosis, String prescriptions);
        Visit changeVisitD(String id, String doctorNotes, String treatmentMade);

        //new functions
        
        void changePassD(String type, String id, String pass);


        //visit

        //return list of all visits
        List<Visit> allVis();
        //return list of all visits by date.
        List<Visit> visitsByDateV();
        Visit getSpecificByIdV(string id);
        //add a visit
        void slectedHourV(Patient pat, int hourIndex, String date);
        Date getAVhoursV(Patient pat, String date);


        

        //Treatment

        List<Treatment> treatmentByDateT();
        Treatment getSpecificByIdT(String id);




        // for all
        void addIT(Object O);

        // doctor + patient
        List<Person> GetOrderListNameP(String instn);
        List<Person> GetOrderListLname(String instn);
        bool ExistId(String id, String instn);
        void newRegister(string pas, string id, string category, string userName);
        Boolean userNameExsist(string newuser, string category);
        void changePas(Person per, string pas);
        Person getByPasAndUserName(string userName, string pass, string category);
    }
}