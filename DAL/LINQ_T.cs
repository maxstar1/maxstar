﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BL_BackHand;

namespace DAL
{
    public class LINQ_T
    {
        public List<Treatment> DBTrea;
        public Master_DAL master;
        public LINQ_T(Master_DAL m){
            DBTrea = new List<Treatment>();
            master = m;

        }
        public void AddIt(Treatment t)
        {
            DBTrea.Add(t);
        }
        public List<Treatment> treaByDate()
        {
            List<Treatment> copy = new List<Treatment>(DBTrea);
            if (copy.Count != 0)
                return copy;
            
                List<Treatment> SortedList = copy.OrderBy(o => o.DateOfStart).ToList();
                return SortedList;
            
        }
        public void remove(Patient p)
        {
            List<Treatment> remove = new List<Treatment>();
            if (DBTrea.Count > 0)
            {
                foreach (Treatment ver in DBTrea)
                {
                    if (ver.pati.id != p.id)
                    {
                        remove.Add(ver);
                    }

                }
                this.DBTrea = remove;
            }
        }
        public Treatment getSpecificById(string id)
        {
            
            if (DBTrea.Count > 0)
            {
                foreach (Treatment trea in DBTrea)
                {
                    if (trea.treatmentID == id)
                    {
                        return trea;
                    }
                }
            }
            return null;

        }

    }

}
